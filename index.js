module.exports = {
    httpLoggerMdw: require('./http-logger'),
    authorizationMdw: require('./authorization'),
    errorHandlerMdw: require('./error-handler')
}
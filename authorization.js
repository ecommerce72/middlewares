const { logger } = require('@ntiendung/logger')
const { response, constants, CommonUtil, JWT } = require('@ntiendung/util')

function checkAuthorization(authRoutes = []) {
    return async (req, res, next) => {
        for (route of authRoutes) {
            const { requireAccessToken, path, method } = route
            const fullRoute = `${process.env.BASE_ROUTE}${path}`
            
            if (CommonUtil.matchPath(fullRoute, req.path) && req.method.toLowerCase() === method.toLowerCase()) {
                logger.debug('Validating route:', req.path)
                if (requireAccessToken) {
                    let authorization = null;

                    // check whether token exist in header or cookie, if not throw error MISSING_AUTHORIZATION
                    if (req.headers && req.headers['authorization']) authorization = req.headers['authorization'];
                    if (req.cookies && req.cookies.authorization) authorization = req.cookies.authorization;

                    //some cases we don't need to authorize 
                    if (!authorization && (requireAccessToken === 'optional' || !requireAccessToken)) return next()

                    if (!authorization && requireAccessToken === 'mandatory')
                        return next(response.getErrResp(constants.MISSING_AUTHORIZATION, undefined, undefined, 403));

                    // get real token to decode
                    const accessToken = authorization.split("Bearer ")[1];

                    try {
                        const user = await JWT.decrypt(accessToken)
                        req.accessToken = accessToken
                        // set decoded data into req for other following layers using later
                        req.user = user;
                        return next()
                    } catch (error) {
                        if (error === 'TokenExpiredError') return next(response.getErrResp(constants.TOKEN_EXPIRED, undefined, undefined, 401));
                        else return next(response.getErrResp(constants.WRONG_AUTHORIZATION_TOKEN, undefined, undefined, 401));
                    }
                }
            }
        }
        return next()
    }
}

module.exports = {
    checkAuthorization
}